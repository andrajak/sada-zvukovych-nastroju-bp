# Bakalářská práce - Sada zvukových nástrojů

Toto repo slouží jako uložistě pro release verze backend a frontend částí systému.

1. **Raspberry Pi image** - Základní verze operačního systému Raspbian (datum verze: 2022-04-04) rozšířená o výše zmíněný backend a potřebné změny v konfiguraci Bluetooth.
2. **APK soubor** - Instalační soubor mobilní aplikace.
3. **3D modely držáků pro mobilní telefony** - Pro snadnější použití doporučuji využít držáku na mobilní zařízení. Jako možný příklad řešení nabízím dva volně dostupné 3D modely.

Backend:
  https://gitlab.fel.cvut.cz/andrajak/audio-system-pi

Frontend:
  https://gitlab.fel.cvut.cz/andrajak/guitarpi-app

**Představení projektu**

Tato práce pojednává o možnostech digitální zvukové syntézy se zaměřením na **hudební efekty**, a to 
způsobem návrhu a implementace systému na zpracování a interpretaci zvukového signálu v reálném 
čase prostřednictvím **Raspberry Pi** v kombinaci s **Android mobilní aplikací**, která slouží jako grafické 
uživatelské rozhraní tohoto systému. Komunikace mezi Raspberry Pi a mobilní aplikací je realizována
pomocí **Bluetooth** technologie. Pro použitelnost řešení je zcela kritická kvalita implementovaných 
efektů a nerozpoznatelně krátká latence celkového zpracování zvuku. Tyto podmínky, a funkčnost 
systému obecně, jsou ověřeny testováním s hudebníky, kteří představují cílovou skupinu uživatelů.

**Obecné schéma zapojení**
![](images/blockSchema.png)

Pro hlavní účel projektu je nutnost vlastnit Raspberry Pi včetně externí zvukové karty, jak je znázorněno na přiložených schémateh. Projekt má dvě verze, které se liší v způsobu interakce s uživatelem. 
1. **Primární** provedení spočívá v již představeném systému, tedy backend běžící na Raspberry Pi 
a frontend je realizován jakožto Android mobilní aplikace.
2. **Sekundární** neboli konzolová verze projektu představuje pouze backend část. Lze ji použít na 
libovolném počítači pod operačním systémem Windows nebo Linux. Ovladatelná je z příkazové 
řádky a slouží zejména k rychlému testování efektů či postprodukci.

Zatímco primární verze nabízí pouze jeden, první režim, konzolová verze nabízí režimy dva:
1. **Playthrough** – editace vstupního signálu a jeho následné přesměrování na výstupní zařízení v 
reálném čase. V případě použití na libovolném zařízení (stolní počítač, notebook, Raspberry Pi) 
je však nutností přítomnost dostatečně flexibilní zvukové karty. Přesněji musí mít DAC, ADC, 
schopnost vzorkovat signál na dostatečné vysoké frekvenci (ideálně až 192 kHz, ale 
použitelných je i 44,1 kHz) a splňovat podmínku minimální bitové hloubky, která je 16 bitů.
2. **Playback** – editace již existujícího signálu a jeho následné přesměrování na výstupní zařízení, 
včetně možnosti uložení nově upraveného záznamu do souboru.

**Primární schéma zapojení**
![](images/realizationSchema.png)
